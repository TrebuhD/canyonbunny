package com.packtub.libgdx.canyonbunny.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.packtub.libgdx.canyonbunny.game.Assets;

public class Clouds extends AbstractGameObject {
    private float length;
    private Array<TextureRegion> mRegionClouds;
    private Array<Cloud> mClouds;

    private class Cloud extends AbstractGameObject {
        private TextureRegion regCloud;

        public Cloud() {}

        public void setRegion (TextureRegion region) {
            regCloud = region;
        }

        @Override
        public void render(SpriteBatch batch) {
            TextureRegion region = regCloud;
            batch.draw(region.getTexture(),
                    position.x + origin.x, position.y + origin.y,
                    origin.x, origin.y,
                    dimension.x, dimension.y,
                    scale.x, scale.y,
                    rotation,
                    region.getRegionX(), region.getRegionY(),
                    region.getRegionWidth(), region.getRegionHeight(),
                    false, false);
        }
    }

    public Clouds (float length) {
        this.length = length;
        init();
    }

    private void init() {
        dimension.set(3.0f, 1.5f);
        mRegionClouds = new Array<TextureRegion>();
        mRegionClouds.add(Assets.instance.mLevelDecoration.cloud01);
        mRegionClouds.add(Assets.instance.mLevelDecoration.cloud02);
        mRegionClouds.add(Assets.instance.mLevelDecoration.cloud03);
        int distanceFactor = 5;
        int numClouds = (int)(length / distanceFactor);
        mClouds = new Array<Cloud>(2 * numClouds);
        for (int i = 0; i < numClouds; i++) {
            Cloud cloud = spawnCloud();
            cloud.position.x = i * distanceFactor;
            mClouds.add(cloud);
        }
    }

    private Cloud spawnCloud() {
        Cloud cloud = new Cloud();
        cloud.dimension.set(dimension);
        // select random cloud image
        cloud.setRegion(mRegionClouds.random());
        Vector2 pos = new Vector2();
        pos.x = length + 10; // position after end of level
        pos.y += 1.75; // base position
        // random extra height
        pos.y += MathUtils.random(0.0f, 0.2f) * (MathUtils.randomBoolean() ? 1 : -1);

        cloud.position.set(pos);
        return cloud;
    }

    @Override
    public void render(SpriteBatch batch) {
        for (Cloud cloud : mClouds) {
            cloud.render(batch);
        }
    }
}
