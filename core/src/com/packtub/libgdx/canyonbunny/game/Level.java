package com.packtub.libgdx.canyonbunny.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.packtub.libgdx.canyonbunny.game.objects.AbstractGameObject;
import com.packtub.libgdx.canyonbunny.game.objects.BunnyHead;
import com.packtub.libgdx.canyonbunny.game.objects.Clouds;
import com.packtub.libgdx.canyonbunny.game.objects.Feather;
import com.packtub.libgdx.canyonbunny.game.objects.GoldCoin;
import com.packtub.libgdx.canyonbunny.game.objects.Mountains;
import com.packtub.libgdx.canyonbunny.game.objects.Rock;
import com.packtub.libgdx.canyonbunny.game.objects.WaterOverlay;

public class Level {
    public static final String TAG = Level.class.getName();

    public enum BLOCK_TYPE {
        EMPTY(0, 0, 0),                     // black
        ROCK(0, 255, 0),                    // green
        PLAYER_SPAWNPOINT(255, 255, 255),   // white
        ITEM_FEATHER(255, 0, 255),          // purple
        ITEM_GOLD_COIN(255, 255, 0);        // yellow

        private int color;

        private BLOCK_TYPE(int r, int g, int b) {
            color = r << 24 | g << 16 | b << 8 | 0xff;
        }

        public boolean sameColor(int color) {
            return this.color == color;
        }

        public int getColor() {
            return color;
        }
    }

    // Objects:
    public Array<Rock> mRocks;
    public Array<GoldCoin> mGoldCoins;
    public Array<Feather> mFeathers;

    // Decoration:
    public Clouds mClouds;
    public Mountains mMountains;
    public WaterOverlay mWaterOverlay;
    public BunnyHead mBunnyHead;

    public Level (String filename) {
        init(filename);
    }

    private void init(String filename) {
        mBunnyHead = null;

        mRocks = new Array<Rock>();
        mGoldCoins = new Array<GoldCoin>();
        mFeathers = new Array<Feather>();

        Pixmap pixmap = new Pixmap(Gdx.files.internal(filename));
        // scan pixels from top-left to bottom-right
        int lastPixel = -1;
        for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
            for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
                AbstractGameObject object = null;
                float offsetHeight = 0;
                // height grows from bottom to top
                float baseHeight = pixmap.getHeight() - pixelY;
                // get color of current pixel
                int currentPixel = pixmap.getPixel(pixelX, pixelY);
                // find matching color value to identify block type at (x, y)
                // point and create the corresponding game obj. if there is a match
                if (BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {
                    // do nothing
                }
                // rock
                else if (BLOCK_TYPE.ROCK.sameColor(currentPixel)) {
                    if (lastPixel != currentPixel) {
                        object = new Rock();
                        float heightIncreaseFactor = 0.25f;
                        offsetHeight = -2.5f;
                        object.position.set(pixelX, baseHeight * object.dimension.y *
                                heightIncreaseFactor + offsetHeight);
                        mRocks.add((Rock)object);
                    } else {
                        mRocks.get(mRocks.size - 1).increaseLength(1);
                    }
                } else if (BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)) {
                    object = new BunnyHead();
                    offsetHeight =-3.0f;
                    object.position.set(pixelX, baseHeight*object.dimension.y
                            + offsetHeight);
                    mBunnyHead = (BunnyHead)object;
                } else if (BLOCK_TYPE.ITEM_FEATHER.sameColor(currentPixel)) {
                    object = new Feather();
                    offsetHeight = -1.5f;
                    object.position.set(pixelX, baseHeight * object.dimension.y
                            + offsetHeight);
                    mFeathers.add((Feather)object);
                } else if (BLOCK_TYPE.ITEM_GOLD_COIN.sameColor(currentPixel)) {
                    object = new GoldCoin();
                    offsetHeight = -1.5f;
                    object.position.set(pixelX, baseHeight * object.dimension.y
                            + offsetHeight);
                    mGoldCoins.add((GoldCoin)object);

                }
                // unknown object / pixel color
                else {
                    int r = 0xff & (currentPixel >>> 24);   // red channel
                    int g = 0xff & (currentPixel >>> 16);   // blue
                    int b = 0xff & (currentPixel >>> 8);    // green
                    int a = 0xff & currentPixel;
                    Gdx.app.error(TAG, "Unknown object at x<" + pixelX
                            + "> y<" + pixelY
                            + ">: r<" + r
                            + "> g<" + g
                            + "> b<" + b
                            + "> a<" + a + ">");
                }
                lastPixel = currentPixel;
            }
        }

        // decoration
        mClouds = new Clouds(pixmap.getWidth());
        mClouds.position.set(0, 2);
        mMountains = new Mountains(pixmap.getWidth());
        mMountains.position.set(-1, -1);
        mWaterOverlay = new WaterOverlay(pixmap.getWidth());
        mWaterOverlay.position.set(0, -3.75f);

        // free memory
        pixmap.dispose();
        Gdx.app.debug(TAG, "level '" + filename + "' loaded");
        }

    public void update(float deltaTime) {
        mBunnyHead.update(deltaTime);
        for (Rock rock : mRocks)
            rock.update(deltaTime);
        for (GoldCoin coin : mGoldCoins)
            coin.update(deltaTime);
        for (Feather feather: mFeathers)
            feather.update(deltaTime);
        mClouds.update(deltaTime);
    }

    public void render (SpriteBatch batch) {
        // Draw mountains
        mMountains.render(batch);
        // Draw rocks
        for (Rock rock : mRocks) {
            rock.render(batch);
        }
        // Draw gold coins
        for (GoldCoin coin : mGoldCoins)
                coin.render(batch);
        // feathers
        for (Feather feather : mFeathers)
                feather.render(batch);
        // player character
        mBunnyHead.render(batch);
        // Draw water
        mWaterOverlay.render(batch);
        // Draw clouds
        mClouds.render(batch);
    }

}
