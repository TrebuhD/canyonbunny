package com.packtub.libgdx.canyonbunny.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.packtub.libgdx.canyonbunny.game.assets.AssetBunny;
import com.packtub.libgdx.canyonbunny.game.assets.AssetFeather;
import com.packtub.libgdx.canyonbunny.game.assets.AssetGoldCoin;
import com.packtub.libgdx.canyonbunny.game.assets.AssetLevelDecoration;
import com.packtub.libgdx.canyonbunny.game.assets.AssetRock;
import com.packtub.libgdx.canyonbunny.util.Constants;

public class Assets implements Disposable, AssetErrorListener {
    private static final String TAG = Assets.class.getName();

    public static final Assets instance = new Assets();
    private AssetManager mAssetManager;

    // Singleton: prevent instantiation from other classes:
    private Assets() {}

    public AssetBunny mBunny;
    public AssetRock mRock;
    public AssetGoldCoin mGoldCoin;
    public AssetFeather mFeather;
    public AssetLevelDecoration mLevelDecoration;
    public AssetFonts mFonts;

    public class AssetFonts {
        public final BitmapFont defaultSmall;
        public final BitmapFont defaultNormal;
        public final BitmapFont defaultBig;

        public AssetFonts() {
            defaultSmall = new BitmapFont(
                    Gdx.files.internal(Constants.FONT_FILE), true);
            defaultNormal = new BitmapFont(
                    Gdx.files.internal(Constants.FONT_FILE), true);
            defaultBig = new BitmapFont(
                    Gdx.files.internal(Constants.FONT_FILE), true);
            // set font sizes
            defaultSmall.setScale(0.75f);
            defaultNormal.setScale(1.0f);
            defaultBig.setScale(2.0f);
            // enable texture filtering
            defaultSmall.getRegion().getTexture().setFilter(
                    Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            defaultNormal.getRegion().getTexture().setFilter(
                    Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            defaultBig.getRegion().getTexture().setFilter(
                    Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
    }

    public void init (AssetManager assetManager) {
        this.mAssetManager = assetManager;
        // set asset manager error handler:
        assetManager.setErrorListener(this);
        // load texture atlas
        try {
            assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS,
                    TextureAtlas.class);
        } catch (Exception e) {
            Gdx.app.error(TAG, "Error loading texture atlas: " + e);
        }
        assetManager.finishLoading();
        Gdx.app.debug(TAG, "# of assets loaded: "
                + assetManager.getAssetNames().size);
        for (String a : assetManager.getAssetNames())
            Gdx.app.debug(TAG, "asset: " + a);

        TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);

        for (Texture t : atlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        mBunny = new AssetBunny(atlas);
        mRock = new AssetRock(atlas);
        mFeather = new AssetFeather(atlas);
        mGoldCoin = new AssetGoldCoin(atlas);
        mLevelDecoration = new AssetLevelDecoration(atlas);
        mFonts = new AssetFonts();

    }

    @Override
    public void dispose() {
        mAssetManager.dispose();
        mFonts.defaultSmall.dispose();
        mFonts.defaultNormal.dispose();
        mFonts.defaultBig.dispose();
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception)throwable);
    }
}
