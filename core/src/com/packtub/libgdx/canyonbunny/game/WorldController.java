package com.packtub.libgdx.canyonbunny.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Rectangle;
import com.packtub.libgdx.canyonbunny.game.objects.BunnyHead;
import com.packtub.libgdx.canyonbunny.game.objects.Feather;
import com.packtub.libgdx.canyonbunny.game.objects.GoldCoin;
import com.packtub.libgdx.canyonbunny.game.objects.Rock;
import com.packtub.libgdx.canyonbunny.util.CameraHelper;
import com.packtub.libgdx.canyonbunny.util.Constants;

public class WorldController extends InputAdapter {
    private static final String TAG = WorldController.class.getName();

    public CameraHelper mCameraHelper;
    public Level mLevel;
    public int lives;
    public int score;

    // rectangles for collision detection
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();

    private void onCollisionBunnyHeadWithRock(Rock rock) {
        BunnyHead bunnyHead = mLevel.mBunnyHead;
        float heightDifference = Math.abs(bunnyHead.position.y
                - rock.position.y
                + rock.bounds.height);
        if (heightDifference > 0.25f) {
            boolean hitLeftEdge = bunnyHead.position.x
                    > ( rock.position.x
                    + rock.bounds.width / 2.0f);
            if (hitLeftEdge) {
                bunnyHead.position.x = rock.position.x
                        + rock.bounds.width;
            } else {
                bunnyHead.position.x = rock.position.x
                        - bunnyHead.bounds.width;
            }
            return;
        }

        switch (bunnyHead.jumpState) {
            case GROUNDED:
                break;
            case FALLING:
            case JUMP_FALLING:
                bunnyHead.position.y = rock.position.y
                        + bunnyHead.bounds.height
                        + bunnyHead.origin.y;
                bunnyHead.jumpState = BunnyHead.JUMP_STATE.GROUNDED;
                break;
            case JUMP_RISING:
                bunnyHead.position.y = rock.position.y
                        + bunnyHead.bounds.height
                        + bunnyHead.origin.y;
            break;
        }
    }

    private void onCollisionBunnyHeadWithGoldCoin(GoldCoin goldCoin) {
        goldCoin.collected = true;
        score += goldCoin.getScore();
        Gdx.app.log(TAG, "Gold coin collected");
    }
    private void onCollisionBunnyHeadWithFeather(Feather feather) {
        feather.collected = true;
        score += feather.getScore();
        mLevel.mBunnyHead.setFeatherPowerup(true);
        Gdx.app.log(TAG, "Feather collected");
    }

    private void testCollisions() {
        r1.set(mLevel.mBunnyHead.position.x,
                mLevel.mBunnyHead.position.y,
                mLevel.mBunnyHead.bounds.width,
                mLevel.mBunnyHead.bounds.height);

        // Test collision: BunnyHead <--> Rocks
        for (Rock rock : mLevel.mRocks) {
            r2.set(rock.position.x, rock.position.y,
                    rock.bounds.width, rock.bounds.height);
            if(!r1.overlaps(r2)) continue;
            onCollisionBunnyHeadWithRock(rock);
            // IMPORTANT: must do all collisions for valid edge testing on rocks.
        }

        // Test collision: BunnyHead <--> Gold Coins
        for (GoldCoin goldCoin : mLevel.mGoldCoins) {
            r2.set(goldCoin.position.x, goldCoin.position.y,
                    goldCoin.bounds.width, goldCoin.bounds.height);
            if(!r1.overlaps(r2)) continue;
            onCollisionBunnyHeadWithGoldCoin(goldCoin);
            break;
        }

        // Test collision: BunnyHead <--> Feathers
        for (Feather feather : mLevel.mFeathers) {
            r2.set(feather.position.x, feather.position.y,
                    feather.bounds.width, feather.bounds.height);
            if(!r1.overlaps(r2)) continue;
            onCollisionBunnyHeadWithFeather(feather);
            break;
        }
    }

    public WorldController() {
        init();
    }

    private void init() {
        Gdx.input.setInputProcessor(this);
        mCameraHelper = new CameraHelper();
        lives = Constants.LIVES_AT_START;
        initLevel();
    }

    public void update(float deltaTime) {
        handleDebugInput(deltaTime);
        mLevel.update(deltaTime);
        testCollisions();
        mCameraHelper.update(deltaTime);
    }

    private void initLevel() {
        score = 0;
        mLevel = new Level(Constants.LEVEL_01);
    }

    private void handleDebugInput(float deltaTime) {
        if (Gdx.app.getType() != Application.ApplicationType.Desktop)
            return;

        // Camera controls (move):
        float camMoveSpeed = 5 * deltaTime;
        float camMoveSpeedAccelerationFactor = 5;
        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
            camMoveSpeed *= camMoveSpeedAccelerationFactor;
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            moveCamera(-camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            moveCamera(camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.UP))
            moveCamera(0, camMoveSpeed);
        if (Gdx.input.isKeyPressed(Keys.DOWN))
            moveCamera(0, -camMoveSpeed);
        if (Gdx.input.isKeyPressed(Keys.BACKSPACE))
            mCameraHelper.setPosition(0, 0);


        // Camera controls (zoom):
        float camZoomSpeed = 1 * deltaTime;
        float camZoomSpeedAccelerationFactor = 5;
        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
            camZoomSpeed *= camZoomSpeedAccelerationFactor;
        if (Gdx.input.isKeyPressed(Keys.COMMA))
            mCameraHelper.addZoom(camZoomSpeed);
        if (Gdx.input.isKeyPressed(Keys.PERIOD))
            mCameraHelper.addZoom(-camZoomSpeed);
        if (Gdx.input.isKeyPressed(Keys.SLASH))
            mCameraHelper.setZoom(1);
    }

    private void moveCamera(float x, float y) {
        x += mCameraHelper.getPosition().x;
        y += mCameraHelper.getPosition().y;
        mCameraHelper.setPosition(x, y);
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Keys.R) {
            init();
            Gdx.app.debug(TAG, "Game world resetted");
        }
        return false;
    }
}
