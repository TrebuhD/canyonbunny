package com.packtub.libgdx.canyonbunny.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import com.packtub.libgdx.canyonbunny.util.Constants;

public class WorldRenderer implements Disposable {
    private static final String TAG = WorldRenderer.class.getName();

    private OrthographicCamera mCamera;
    private OrthographicCamera cameraGUI;
    private SpriteBatch mBatch;
    private WorldController mWorldController;

    public WorldRenderer (WorldController worldController) {
        this.mWorldController = worldController;
        init();
    }

    private void init() {
        mBatch = new SpriteBatch();
        mCamera = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
        mCamera.position.set(0, 0, 0);
        mCamera.update();

        cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH,
                Constants.VIEWPORT_GUI_HEIGHT);
        cameraGUI.position.set(0, 0, 0);
        cameraGUI.setToOrtho(true);
        cameraGUI.update();
    }

    private void renderGuiScore(SpriteBatch batch) {
        float x = -15;
        float y = -15;
        batch.draw(Assets.instance.mGoldCoin.goldCoin,
                x, y, 50, 50, 100, 100, 0.35f, -0.35f, 0);
        Assets.instance.mFonts.defaultBig.draw(batch,
                "" + mWorldController.score,
                x + 75, y + 37);
    }

    private void renderGuiExtraLife (SpriteBatch batch) {
        float x = cameraGUI.viewportWidth - 50
                - Constants.LIVES_AT_START * 50;
        float y = -15;
        for (int i = 0; i < Constants.LIVES_AT_START; i++) {
            if (mWorldController.lives <= i)
                batch.setColor(0.5f, 0.5f, 0.5f, 0.5f);
            batch.draw(Assets.instance.mBunny.head,
                    x + i * 50, y, 50, 50, 120, 100, 0.35f, -0.35f, 0);
            batch.setColor(1, 1, 1, 1);
        }
    }

    private void renderGuiFpsCounter (SpriteBatch batch) {
        float x = cameraGUI.viewportWidth - 55;
        float y = cameraGUI.viewportHeight - 15;
        int fps = Gdx.graphics.getFramesPerSecond();
        BitmapFont fpsFont = Assets.instance.mFonts.defaultNormal;
        if (fps >= 45) {
            fpsFont.setColor(0, 1, 0, 1);
        } else if (fps >= 30) {
            fpsFont.setColor(1, 1, 0, 1);
        } else {
            fpsFont.setColor(1, 0, 0, 1);
        }
        fpsFont.draw(batch, "FPS: " + fps, x, y);
        fpsFont.setColor(1, 1, 1, 1);
    }

    private void renderWorld(SpriteBatch batch) {
        mWorldController.mCameraHelper.applyTo(mCamera);
        batch.setProjectionMatrix(mCamera.combined);
        batch.begin();
        mWorldController.mLevel.render(batch);
        batch.end();
    }

    private void renderGui(SpriteBatch batch) {
        batch.setProjectionMatrix(cameraGUI.combined);
        batch.begin();
        renderGuiScore(batch);
        renderGuiExtraLife(batch);
        renderGuiFpsCounter(batch);
        batch.end();
    }

    public void render() {
        renderWorld(mBatch);
        renderGui(mBatch);
    }

    public void resize(int width, int height) {
        mCamera.viewportWidth = (Constants.VIEWPORT_HEIGHT / height) * width;
        mCamera.update();

        cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / (float) height * (float)width);
        cameraGUI.viewportHeight = (Constants.VIEWPORT_GUI_HEIGHT);
        cameraGUI.position.set(cameraGUI.viewportWidth / 2, cameraGUI.viewportHeight /2, 0);
        cameraGUI.update();
    }

    @Override
    public void dispose() {
        mBatch.dispose();
    }
}
