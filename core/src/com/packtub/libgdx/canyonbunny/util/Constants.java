package com.packtub.libgdx.canyonbunny.util;

public class Constants {
    //Visible game world 5 meters wide
    public static final float VIEWPORT_WIDTH = 5.0f;

    //and 5 meters tall
    public static final float VIEWPORT_HEIGHT = 5.0f;

    public static final String ANDROID_EXTRA_PATH = "android/assets/";

    public static final String FONT_FILE = ANDROID_EXTRA_PATH + "images/arial-15.fnt";

    public static final String TEXTURE_ATLAS_OBJECTS = ANDROID_EXTRA_PATH + "images/canyonbunny.pack";

    public static final String LEVEL_01 = ANDROID_EXTRA_PATH + "levels/level-01.png";

    public static final float VIEWPORT_GUI_WIDTH = 800.0f;

    public static final float VIEWPORT_GUI_HEIGHT = 480.0f;

    public static final int LIVES_AT_START = 3;

    public static final float ITEM_FEATHER_POWERUP_DURATION = 9;

}
