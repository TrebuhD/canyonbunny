package com.packtub.libgdx.canyonbunny;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.packtub.libgdx.canyonbunny.game.Assets;
import com.packtub.libgdx.canyonbunny.game.WorldController;
import com.packtub.libgdx.canyonbunny.game.WorldRenderer;


public class CanyonBunnyMain extends ApplicationAdapter {
    private static final String TAG = CanyonBunnyMain.class.getName();

    private WorldController mWorldController;
    private WorldRenderer mWorldRenderer;
    private boolean mPaused;

	SpriteBatch batch;
	Texture img;
	
	@Override
	public void create () {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        Assets.instance.init(new AssetManager());

        mWorldController = new WorldController();
        mWorldRenderer = new WorldRenderer(mWorldController);
        mPaused = false;
	}

	@Override
	public void render () {
        if (!mPaused) {
            mWorldController.update(Gdx.graphics.getDeltaTime());
        }
        // set clear screen color to blue
        Gdx.gl.glClearColor(0x64/255.0f, 0x95/255.0f, 0xed/255.0f, 0xff/255.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mWorldRenderer.render();
	}

    @Override
    public void resume() {
        Assets.instance.init(new AssetManager());
        mPaused = false;
    }

    @Override
    public void resize(int width, int height) {
        mWorldRenderer.resize(width, height);
    }

    @Override
    public void dispose() {
        mWorldRenderer.dispose();
        Assets.instance.dispose();
    }

    @Override
    public void pause() {
        mPaused = true;
    }
}
